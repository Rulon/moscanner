# MoScanner

MoScanner is an Android application that looks for all the Bluetooth Low Energy (BLE) devices in range of the device and shows their characteristics to the user.

**Note:** Sections marked in *italics* are pending or incomplete. See the Pending section for details.

## Architecture

The app uses the Model-View-ViewModel (MVVM) architecture in order to separate the app into modular layers.

### Model

The Model layer contains all classes in charge of storing the app's data, as well as classes that fetch data from external sources, such as Bluetooth connections. This particular Model includes two clases that store the information received through Bluetooth. One class, called Device, represents a Bluetooth service identified by its uuid. Each Device also contains a list of instances of the second class, Characteristic, which represents the different Bluetooth characteristics available in each service.

The app's Model also includes a third class called BluetoothConnector, which is in charge of communicating with external devices and storing their data in the app. When told to look for devices, it uses a separate thread to *search for nearby Bluetooth connections*. A thread different from the main thread is used to prevent blocking the app while the search is done. Until the thread is told to stop looking for connections, it searches for them once every 10 seconds. 10 seconds were chosen for the time interval since it is a reasonable amount of time to wait for an update on the connections available. It would serve no purpose to have the thread looking for connections at all times, since it would be a waste of resources, and it's very unlikely for the available connections to change a lot in less than 10 seconds.

When working with this architecture it is a good practice to have a centralized class from where the ViewModel gets its data. This class is often called a Repository, and it's part of the Model. The Repository collects all the data from every different source and acts as an intermediate between these sources and the ViewModel. Since this app was designed to have only one data source (the Bluetooth connections), a Repository is not needed an thus was not created. If, however, a second data source (such as a database) was added to the app in the future, I would recommend to create a Repository to manage the flow of data between the BluetoothConnector class, the ViewModel, and this second source.

### View

The View layer's purpose is to show data to the user and handle user interaction. The app's view consists of *a single Activity with two Fragments*. The first fragment is shown to the user as soon as the app is started, and consists of a RecyclerView with a *list of available Bluetooth devices*. A RecyclerView was chosen to be resource efficient, since the device list might grow or shrink over time.

*The second fragment* will contain the service's description and a list of its characteristics. This fragment will be shown when the user selects a device from the list in the first fragment, displaying the respective information.

### ViewModel

The ViewModel is in charge of facilitating communication between the Model and the View, as well as doing any data processing necessary. Since this app consists of mainly sending data from the Model and showing it in the View, with no complex processing needed, its ViewModel is very simple. It has methods that can be called when the user triggers an event in the View, and tells the Model to act accordingly.

### Observer-subscriber pattern

The MVVM architecture requires constant communication between the three layers, but also requires that the only reference the View can have is one of the ViewModel, and the only one the ViewModel can have is one of the Model, not viceversa. So, a way of notifying the View when data changes, other than calling a View method directly, is needed.

The RxJava library was chosen for this task. It defines an Observable object that is bound to some data, and emits this data to all objects subscribed to it, called Observers. In this app, the Observable object is part of the BluetoothConnector class, and *emits the list of Devices that are available*. The Observable is configured so that it sends the data repeatedly every 10 seconds, thus reporting any change made to the connection list by the search thread correctly.

The Observer object is defined in the View's only activity. Right now it prints the values sent by the Observable to the console. Once the rest of the View is implemented, it *will update the RecyclerView in the first Fragment to show the respective data*. This will be done by comparing the ids of the Devices currently shown in the RecyclerView with the Devices sent by the Observable. Any Devices included in the list that are not being shown in the view will be added, and any Devices show in the view hat are no longer in the list will be removed.

## Installation

Download this repository and import the files to an Android Studio project. Be sure to have all the dependencies required by the Gradle file installed.

## Usage

Run the MainActivity in Android Studio on a simulated or physical device. You should see a blank screen, but messages will be shown in the Andoird Studio console. The app is configured to start sending data from the Model to the View, by simulating new connections with a random numbers. A random number is generated every 10 seconds by the BluetoothConnector class, and is added to the list. The list is then sent to the View where it is printed.

## Pending

**BLE processing library:** The app does not look for Bluetooth connections at the moment. It generates random numbers instead. In the future, when BLE processing is added, the random number generator will be replaced with it. All of the app's architecture was made taking this into account so that with the addition of the BLE processing it's not necessary to change any other parts of the app.

**UI:** An Activity with a Fragment and a RecyclerView is implemented. However, there are still no buttons and other UI elements with which the user can interact. Also, the second Fragment that shows a service's description and characteristics is still pending. For now, the View prints to the console the data sent by the Model instead of showing it in the UI.

**Dependency Injection:** The app currently implements references to other objects the traditional way: An object is owned as an attribute and instantiated in the constructor (for example, the ViewModel does this with its BluetoothConnector object). I'm aware this is not ideal since it violates the Inversion of Control principle. The plan is to use the Dagger2 dependency injector to solve this issue.