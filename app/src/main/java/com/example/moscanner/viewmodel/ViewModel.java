package com.example.moscanner.viewmodel;

import com.example.moscanner.models.BluetoothConnector;

import io.reactivex.Observable;

/* The ViewModel is the intermediate class between the View (UI) and the Model (data).
 * It's a singleton because only one instance of itself is needed, and includes methods
 * for communication between these two other layers.
 * NOTE: This class contains a direct reference to the BluetoothConnector class and gets
 * an instance of it in the constructor. This is not ideal since it violates the principle
 * of inversion of control. It will be corrected once dependency injection is implemented.
 */
public class ViewModel {
    private BluetoothConnector mConnector;
    private static ViewModel sInstance;

    private ViewModel() {
        mConnector = BluetoothConnector.getInstance();
    }

    public static ViewModel getInstance() {
        if (sInstance == null) {
            sInstance = new ViewModel();
        }
        return sInstance;
    }

    public Observable getDeviceObservable() {
        return mConnector.getDeviceObservable();
    }

    public void findBluetoothConnections() {
        mConnector.findConnections();
    }

    public void stopFindingBluetoothConnections() {
        mConnector.stopFindingConnections();
    }
}
