package com.example.moscanner.models;

import java.util.ArrayList;

/* The Device model represents a Bluetooth device in range of the device running the app.
 * It has the following attributes: a name, an ID, a boolean indicating if it's accepting
 * connections, the manufacturer data, the local name, and a list of Characteristics.
 */
public class Device {
    private String mName;
    private boolean mConnectable;
    private String mLocalName;
    private String mManufacturerData;
    private String mUuid;
    private ArrayList<Characteristic> mCharacteristics;

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public boolean isConnectable() {
        return mConnectable;
    }

    public void setConnectable(boolean connectable) {
        this.mConnectable = connectable;
    }

    public String getLocalName() {
        return mLocalName;
    }

    public void setLocalName(String localName) {
        this.mLocalName = localName;
    }

    public String getManufacturerData() {
        return mManufacturerData;
    }

    public void setManufacturerData(String manufacturerData) {
        this.mManufacturerData = manufacturerData;
    }

    public String getUuid() {
        return mUuid;
    }

    public void setUuid(String uuid) {
        this.mUuid = uuid;
    }

    public ArrayList<Characteristic> getCharacteristics() {
        return mCharacteristics;
    }

    public void setCharacteristics(ArrayList<Characteristic> characteristics) {
        this.mCharacteristics = characteristics;
    }
}
