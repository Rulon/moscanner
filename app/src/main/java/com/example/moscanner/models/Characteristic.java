package com.example.moscanner.models;

/* The Characteristic model represents a Bluetooth characteristic contained in a device.
 * It has an ID, a value, and an access level. An enum was chosen to represent access
 * levels because there are predefined values for the levels (READ, WRITE, and READ/WRITE).
 */
public class Characteristic {
    public enum AccessLevel {
        READ,
        WRITE,
        READWRITE
    }

    private String mUuid;
    private AccessLevel mAccessLevel;
    private String mValue;

    public String getUuid() {
        return mUuid;
    }

    public void setUuid(String uuid) {
        this.mUuid = uuid;
    }

    public AccessLevel getAccessLevel() {
        return mAccessLevel;
    }

    public void setAccessLevel(AccessLevel accessLevel) {
        this.mAccessLevel = accessLevel;
    }

    public String getValue() {
        return mValue;
    }

    public void setValue(String value) {
        this.mValue = value;
    }
}