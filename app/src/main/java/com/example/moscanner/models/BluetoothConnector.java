package com.example.moscanner.models;

import java.util.Random;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;

/* BluetoothConnector will handle all BLE connectivity. This class is a Singleton because
 * only one instance is required, and having several instances could seriously damage the
 * information flow. It has several attributes: a ConcurrentSkipListMap that contains all
 * the devices in range of the app, an RxJava Observable that emits this list of devices,
 * a Thread in charge of finding nearby Bluetooth devices, and a boolean that serves as a
 * flag to tell the Thread when to stop.
 */
public class BluetoothConnector {
    private ConcurrentSkipListMap<String, Device> mDeviceList;
    private static BluetoothConnector sInstance;
    private Observable mDeviceObservable;
    private Thread mBluetoothFinderThread;
    private boolean mKeepSearching;

    private BluetoothConnector() {
        mDeviceList = new ConcurrentSkipListMap<String, Device>();
        mDeviceObservable = Observable.just(mDeviceList).delay(10, TimeUnit.SECONDS).repeat();
        mKeepSearching = false;
        mBluetoothFinderThread = new Thread() {
            @Override
            public void run() {
                try {
                    while (mKeepSearching) {
                        searchForConnections();
                        Thread.sleep(10000);
                    }
                }catch(InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
    }

    public static BluetoothConnector getInstance() {
        if (sInstance == null) {
            sInstance = new BluetoothConnector();
        }
        return sInstance;
    }

    public Observable getDeviceObservable() {
        return mDeviceObservable;
    }

    /* Reset the flag and start the Thread that looks for connections.
     */
    public void findConnections() {
        mKeepSearching = true;
        mBluetoothFinderThread.start();
    }

    /* Signal the Thread to stop by changing the flag.
     */
    public void stopFindingConnections() {
        mKeepSearching = false;
    }

    /* Will look for Bluetooth connections in range. For now generates random
     * numbers and adds them to the device list so that the app can be tested.
     */
    private void searchForConnections() {
        Random r = new Random();
        mDeviceList.put(r.nextInt() + "", new Device());
    }
}
