package com.example.moscanner.views;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.moscanner.R;
import com.example.moscanner.models.Device;
import com.example.moscanner.viewmodel.ViewModel;

import java.util.Iterator;
import java.util.NavigableSet;
import java.util.concurrent.ConcurrentSkipListMap;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class MainActivity extends AppCompatActivity {
    private ViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mViewModel = ViewModel.getInstance();
        mViewModel.getDeviceObservable().subscribe(new Observer<ConcurrentSkipListMap<String, Device>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(ConcurrentSkipListMap<String, Device> map) {
                check(map);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
        mViewModel.findBluetoothConnections();
    }

    /* Prints the keys of all values in the Device list sent by the Model
     */
    private void check(ConcurrentSkipListMap map) {
        NavigableSet keys = map.navigableKeySet();
        for (Iterator iter = keys.iterator(); iter.hasNext();) {
            System.out.println(iter.next());
        }
    }
}
