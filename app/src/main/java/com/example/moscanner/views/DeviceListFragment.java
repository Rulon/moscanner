package com.example.moscanner.views;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.moscanner.R;


/* The DeviceListFragment will include a RecyclerView displaying every Device
 * in range, and will be updated according to the device list in the model.
 */
public class DeviceListFragment extends Fragment {

    public DeviceListFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_device_list, container, false);
    }
}
